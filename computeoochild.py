#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys

import computeoo

class ComputeChild(computeoo.Compute):
    def set_def(self):
        if self.exp <= 0 :
            try:
                math.log(self.op1, self.exp)
            except BaseException:
                print("NO se admite 0 como base")
            raise ValueError("NO se admite 0 como base")

    def get_def(self):
        if sys.argv[1] == "power":
            print("Potencia de tu operación es = " + str(num2))
        elif sys.argv[1] == "log":
            print("Base de tu operación es = "+ str(num2))

if __name__ == "__main__":
    if len(sys.argv) > 4:
        sys.exit("Error: Only the first 2 will be taken into account.")


    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = 2
    if len(sys.argv) == 4:
        num2 = float(sys.argv[3])
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        objeto = ComputeChild(num, num2)
        objeto.power()
        objeto.get_def()

    elif sys.argv[1] == "log":
        objeto = ComputeChild(num, num2)
        if num2 == 0:
            objeto.set_def()
        else:
            objeto.log()
            objeto.get_def()
    else:
        sys.exit('Operand should be power or log')


