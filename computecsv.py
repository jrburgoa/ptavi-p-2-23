#!/usr/bin/python3
# -*- coding: utf-8 -*-

import computecount


def process_csv(fichero: str):
    with open(fichero, 'r') as fich:
        txt = fich.read().split()
        cuenta = 0
        calculo = computecount.Compute()
        for linea in txt:
            linea_actual = linea.split(',')

            if cuenta == 0:
                try:
                    defa = float(linea_actual[0])
                except ValueError:
                    print('Bad format')
                else:
                    if defa <= 0:
                        raise ValueError('Error: default value must be greater than zero')
                    else:
                        calculo.default = float(linea_actual[0])
                        print("Default: "+str(calculo.default))

            else:
                operacion = linea_actual[0]
                if (operacion != 'power') and (operacion != 'log'):
                    print('Bad format')
                else:
                    try:
                        num = float(linea_actual[1])
                    except ValueError:
                        print('Bad format')
                    else:
                        if len(linea_actual) == 2:
                            if operacion == 'power':
                                print(calculo.power(num, calculo.default))
                            elif operacion == 'log':
                                print(calculo.log(num, calculo.default))

                        elif len(linea_actual) == 3:
                            try:
                                num2 = float(linea_actual[2])
                            except ValueError:
                                print('Bad format')
                            else:
                                if operacion == 'power':
                                    print(calculo.power(num, num2))
                                elif operacion == 'log':
                                    print(calculo.log(num, num2))
                        elif len(linea_actual) > 3:
                            print('Bad format')

            cuenta += 1
        print("Operations: "+str(calculo.count))
    fich.close()


if __name__ == '__main__':
    process_csv('operations.csv')
