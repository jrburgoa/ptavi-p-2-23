import math
import sys

class Compute():
    def __init__(self, op1,exp):
        self.op1 = op1
        self.exp = exp

    def power(self):
        x = self.op1 ** self.exp
        print("Tu resultado es = " + str(x))

    def log(self):
        print("Tu resultado es = " + str(math.log(self.op1, self.exp)))



if __name__ == "__main__":
    if len(sys.argv) > 4:
        sys.exit("Error: Only the first 2 will be taken into account.")


    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = 2
    if len(sys.argv) == 4:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        objeto = Compute(num, num2)
        objeto.power()
    elif sys.argv[1] == "log":
        objeto = Compute(num, num2)
        objeto.log()
    else:
        sys.exit('Operand should be power or log')


